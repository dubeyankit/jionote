package com.jio.mhood.jionote;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ankit.dubey on 12/5/14.
 */
public class Utils {

    public static String getLastEditedString(long time){
        String formattedDate = "";
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(time));
        int year = c.get(Calendar.YEAR);
        int month  = c.get(Calendar.MONTH);
        int day  = c.get(Calendar.DAY_OF_MONTH);
        c.setTime(new Date(System.currentTimeMillis()));
        if(year== c.get(Calendar.YEAR)){
            if(month == c.get(Calendar.MONTH)){
                if(day == c.get(Calendar.DAY_OF_MONTH)){// Today
                    SimpleDateFormat df = new SimpleDateFormat("hh:mm:a");
                    formattedDate = "Today "+df.format(time);
                }
                else if((c.get(Calendar.DAY_OF_MONTH)-day)==1){// Yesterday
                    SimpleDateFormat df = new SimpleDateFormat("hh:mm:a");
                    formattedDate = "Yesterday "+df.format(time);
                }
                else{
                    SimpleDateFormat df = new SimpleDateFormat("MMM-dd");
                    formattedDate = df.format(time);
                }
            }
            else{
                SimpleDateFormat df = new SimpleDateFormat("MMM-dd");
                formattedDate = df.format(time);
            }
        }
        else{
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            formattedDate = df.format(time);
        }
        return formattedDate;
    }

    public static String getTimeString(long time){
        String formattedDate = "";
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(time));
        int year = c.get(Calendar.YEAR);
        int month  = c.get(Calendar.MONTH);
        int day  = c.get(Calendar.DAY_OF_MONTH);
        c.setTime(new Date(System.currentTimeMillis()));
        if(year== c.get(Calendar.YEAR)){
            if(month == c.get(Calendar.MONTH)){
                if(day == c.get(Calendar.DAY_OF_MONTH)){// Today
                    SimpleDateFormat df = new SimpleDateFormat("hh:mm:a");
                    formattedDate = "Today "+df.format(time);
                }
                else if((c.get(Calendar.DAY_OF_MONTH)-day)==1){// Yesterday
                    SimpleDateFormat df = new SimpleDateFormat("hh:mm:a");
                    formattedDate = "Yesterday "+df.format(time);
                }
                else{
                    SimpleDateFormat df = new SimpleDateFormat("MMM-dd");
                    formattedDate = df.format(time);
                }
            }
            else{
                SimpleDateFormat df = new SimpleDateFormat("MMM-dd");
                formattedDate = df.format(time);
            }
        }
        else{
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            formattedDate = df.format(time);
        }
        return formattedDate;
    }
}
