package com.jio.mhood.jionote.cache;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ankit.dubey on 8/20/14.
 */
public class SimpleJioNote extends JioNote implements Parcelable{

    public SimpleJioNote(String title, String note, int backGroundColor, boolean isSticky, boolean isDeviceOnly) {
        super(title, note, backGroundColor,isSticky, isDeviceOnly);
        noteType = NOTE_SIMPLE;
    }

    public SimpleJioNote(Parcel in){
        fromJSON(in.readString());
        setTargetStore(in.readInt());
        setBitName(in.readString());
        setVersion(in.readInt());
        noteType = NOTE_SIMPLE;
    }

    public SimpleJioNote() {
        noteType = NOTE_SIMPLE;
    }
    public String toString(){
        return toJSON();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<SimpleJioNote> CREATOR = new Parcelable.Creator<SimpleJioNote>() {
        public SimpleJioNote createFromParcel(Parcel in) {
            return new SimpleJioNote(in);
        }

        public SimpleJioNote[] newArray(int size) {
            return new SimpleJioNote[size];
        }
    };


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(toJSON());
        parcel.writeInt(getTargetStore());
        parcel.writeString(getBitName());
        parcel.writeInt(getVersion());
    }
}
