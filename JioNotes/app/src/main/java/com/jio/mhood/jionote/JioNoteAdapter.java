package com.jio.mhood.jionote;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jio.mhood.jionote.activities.JioNotesMainActivity;
import com.jio.mhood.jionote.cache.JioNote;
import com.jio.mhood.services.api.bitstore.common.BitStoreTarget;

/**
 * Created by ankit.dubey on 11/14/14.
 */
public class JioNoteAdapter extends ArrayAdapter<JioNote>{
    private JioNotesMainActivity context;
    private JioNote[] notes;
    private boolean displayDelete = false;

    public JioNoteAdapter(JioNotesMainActivity context, JioNote[] notes) {
        super(context, R.layout.jionote_row_layout, notes);
        this.context = context;
        this.notes = notes;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.jionote_row_layout, parent, false);
        JioNoteApplication application = (JioNoteApplication) context.getApplication();
        rowView.setBackgroundColor(notes[position].getBackGroundColor());
        TextView title = (TextView) rowView.findViewById(R.id.noteTitle);
        title.setText(notes[position].getTitle());
        TextView note = (TextView) rowView.findViewById(R.id.noteValue);
        note.setText(notes[position].getNote());
        application.setTypeface(title);
        application.setTypeface(note);
        ImageView type = (ImageView) rowView.findViewById(R.id.type);
        switch(notes[position].getTargetStore()){
            case BitStoreTarget.BitStore_APP_USER:
                type.setImageResource(R.drawable.ic_action_person);
                break;
            case BitStoreTarget.BitStore_APP:
                type.setImageResource(R.drawable.ic_action_web_site);
                break;
            case BitStoreTarget.BitStore_APP_USER_DEVICE:
                type.setImageResource(R.drawable.ic_action_phone);
                break;
        }
        TextView lastEdited = (TextView) rowView.findViewById(R.id.lastEdited);
        lastEdited.setText(Utils.getLastEditedString(notes[position].getUpdatedAt()));

        ImageView delete = (ImageView) rowView.findViewById(R.id.delete);
        delete.setTag(notes[position]);
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                context.deleteNote((JioNote)view.getTag());
            }
        });
        if(displayDelete)
            delete.setVisibility(View.VISIBLE);
        else
            delete.setVisibility(View.GONE);
        LinearLayout noteLayout = (LinearLayout) rowView.findViewById(R.id.noteLayout);
        noteLayout.setTag(notes[position]);
        noteLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                context.viewNote((JioNote)view.getTag());
            }
        });
        noteLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                displayDelete = !displayDelete;
                notifyDataSetInvalidated();
                return true;
            }
        });

        return rowView;
    }

    public JioNote getNote(int position){
        return notes[position];
    }

    public boolean isDisplayDelete(){
        return  displayDelete;
    }

    public void disableDisplayDelete(){
        displayDelete = false;
        notifyDataSetInvalidated();
    }
}
