package com.jio.mhood.jionote.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jio.mhood.services.api.bitstore.common.BitStoreTarget;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class JioNotesDBHelper extends SQLiteOpenHelper {

    private static final String TABLE_NOTES = "notes";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_NOTE_JSON = "json";
    private static final String COLUMN_NOTE_KEY = "key";
    private static final String COLUMN_NOTE_VERSION = "version";
    private static final String COLUMN_TARGET_STORE = "target_store";

    private static final String DATABASE_NAME = "jionotes.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = JioNotesDBHelper.class
            .getSimpleName();

    private Context context;
    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NOTES + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_NOTE_JSON + " text not null,"
            + COLUMN_NOTE_KEY + " text not null,"
            + COLUMN_NOTE_VERSION + " integer not null,"
            + COLUMN_TARGET_STORE + " integer not null"
            +");";

    private JioNotesDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
    private static JioNotesDBHelper sSingleton = null;

    public static synchronized JioNotesDBHelper getInstance(Context context) {

        if (null == sSingleton) {
            sSingleton = new JioNotesDBHelper(context);
        }
        return sSingleton;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        android.util.Log.d(TAG,
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
        onCreate(db);
    }

    public void deleteDB(){
        try {
            Log.d(TAG,"Delete DB");
            context.deleteDatabase(DATABASE_NAME);
        } catch(Exception e) {

        }
    }


    public JioNote addNote(JioNote note) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NOTE_JSON, note.toJSON());
        values.put(COLUMN_NOTE_KEY, note.getBitName());
        values.put(COLUMN_NOTE_VERSION, note.getVersion());
        values.put(COLUMN_TARGET_STORE, note.getTargetStore());
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert(JioNotesDBHelper.TABLE_NOTES, null,
                values);
        return note;
    }

    public JioNote updateNote(JioNote note) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NOTE_JSON, note.toJSON());
        values.put(COLUMN_NOTE_VERSION, note.getVersion());
        values.put(COLUMN_TARGET_STORE, note.getTargetStore());
        SQLiteDatabase database = this.getWritableDatabase();
        database.update(TABLE_NOTES, values, COLUMN_NOTE_KEY + "= '" + note.getBitName() + "'", null);
        return note;
    }

    public void addNotes(ArrayList<JioNote> notes) {
        for(JioNote note:notes){
            addNote(note);
        }
    }

    public void deleteNote(JioNote note) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(JioNotesDBHelper.TABLE_NOTES, COLUMN_NOTE_KEY
                + " = '" + note.getBitName()+"'", null);
        android.util.Log.d(TAG, "Note deleted: " + note.toString());
    }

    private void deleteAllNote(int selectedStore) {
        SQLiteDatabase database = this.getWritableDatabase();
        //database.delete(JioNotesDBHelper.TABLE_NOTES, null, null);
        database.delete(JioNotesDBHelper.TABLE_NOTES, COLUMN_TARGET_STORE
                + " = " + selectedStore+"", null);
        android.util.Log.d(TAG, "All Notes deleted of Selected Store. ");
    }
    public void deleteAllNote() {
        deleteAllNote(BitStoreTarget.BitStore_APP);
        deleteAllNote(BitStoreTarget.BitStore_APP_USER);
        deleteAllNote(BitStoreTarget.BitStore_APP_USER_DEVICE);
    }

    private ArrayList<JioNote> getAllNotes(int selectedStore) {
        ArrayList<JioNote> notes = new ArrayList<JioNote>();
        String selectQuery = "SELECT  * FROM " + TABLE_NOTES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {

                String jsonStr = (c.getString(c.getColumnIndex(COLUMN_NOTE_JSON)));
                String key = (c.getString(c.getColumnIndex(COLUMN_NOTE_KEY)));
                int version = (c.getInt(c.getColumnIndex(COLUMN_NOTE_VERSION)));
                int targetStore = (c.getInt(c.getColumnIndex(COLUMN_TARGET_STORE)));
                if(targetStore != selectedStore)
                    continue;
                JioNote note = null;
                try {
                    JSONObject json = new JSONObject(jsonStr);
                    int noteType = json.getInt("noteType");
                    switch (noteType) {
                        case JioNote.NOTE_SIMPLE:
                            note = new SimpleJioNote();
                            note.fromJSON(jsonStr);

                            break;
                        case JioNote.NOTE_LIST:
                            note = new ListJioNote();
                            note.fromJSON(jsonStr);
                            break;
                    }
                    Log.d(TAG, "getAllNotes():-->" + note.toString());
                    Log.d(TAG, "getAllNotes():--> Version" + note.toString());
                    note.setBitName(key);
                    note.setTargetStore(targetStore);
                    note.setVersion(version);
                    notes.add(note);
                }
                catch(JSONException e){

                }
            } while (c.moveToNext());
        }
        Collections.sort(notes,new Comparator<JioNote>() {
            @Override
            public int compare(JioNote jioNote, JioNote jioNote2) {
                return (int)(jioNote2.getUpdatedAt()-jioNote.getUpdatedAt());
            }
        });
        // Separate out Sticky notes....
        ArrayList<JioNote> nonSticky = new ArrayList<JioNote>();
        ArrayList<JioNote> sticky = new ArrayList<JioNote>();
        for(JioNote note: notes){
            if(!note.isSticky()){
                nonSticky.add(note);
            }else{
                sticky.add(note);
            }
        }
        notes.clear();
        notes.addAll(sticky);
        notes.addAll(nonSticky);
        return notes;
    }

    public ArrayList<JioNote> getAllNotes() {
        ArrayList<JioNote> notes = new ArrayList<JioNote>();
        notes.addAll(getAllNotes(BitStoreTarget.BitStore_APP));
        notes.addAll(getAllNotes(BitStoreTarget.BitStore_APP_USER));
        notes.addAll(getAllNotes(BitStoreTarget.BitStore_APP_USER_DEVICE));
        Collections.sort(notes,new Comparator<JioNote>() {
            @Override
            public int compare(JioNote jioNote, JioNote jioNote2) {
                return (int)(jioNote2.getUpdatedAt()-jioNote.getUpdatedAt());
            }
        });
        // Separate out Sticky notes....
        ArrayList<JioNote> nonSticky = new ArrayList<JioNote>();
        ArrayList<JioNote> sticky = new ArrayList<JioNote>();
        for(JioNote note: notes){
            if(!note.isSticky()){
                nonSticky.add(note);
            }else{
                sticky.add(note);
            }
        }
        notes.clear();
        notes.addAll(sticky);
        notes.addAll(nonSticky);
        return notes;
    }
}
