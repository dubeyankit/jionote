package com.jio.mhood.jionote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.jio.mhood.jionote.cache.JioNotesDBHelper;
import com.jio.mhood.services.api.accounts.authentication.AuthenticationManager;

public class MDKBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String message = null;
        if (AuthenticationManager.ACTION_LOGIN_FINISHED.equals(action)) {
            boolean result = intent.getBooleanExtra(AuthenticationManager.INTENT_EXTRA_JIO_RESULT,
                    false);
            message = "Login finished! Result: " + result;
        } else if (AuthenticationManager.ACTION_LOGOUT_FINISHED.equals(action)) {
            boolean result = intent.getBooleanExtra(AuthenticationManager.INTENT_EXTRA_JIO_RESULT,
                    false);
            final Context appContext = context.getApplicationContext();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    JioNotesDBHelper.getInstance(appContext).deleteAllNote();
                    Log.d("JioNote-->MDKBroadcastReceiver","User Logged Out.All Notes Deleted");
                    //Toast.makeText(context, "User Logged Out.All Notes Deleted", Toast.LENGTH_LONG).show();
                }
            }).start();

            message = "Logout finished! Result: " + result;
        } else if (AuthenticationManager.ACTION_SSO_TOKEN_CHANGED.equals(action)) {
            message = "SSO Token changed!";
        }
        Log.d("JioNote-->MDKBroadcastReceiver",message);
        //Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}
