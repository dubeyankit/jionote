package com.jio.mhood.jionote.cache;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ankitdubey on 8/20/14.
 */
public class ListJioNote extends JioNote implements Parcelable{

    public ListJioNote(String title, String note, int backGroundColor, boolean isSticky, boolean isDeviceOnly) {
        super(title, note, backGroundColor,isSticky, isDeviceOnly);
        noteType = NOTE_LIST;
    }

    public ListJioNote(Parcel in){
        fromJSON(in.readString());
        setTargetStore(in.readInt());
        setBitName(in.readString());
        setVersion(in.readInt());
    }

    public static final Parcelable.Creator<ListJioNote> CREATOR = new Parcelable.Creator<ListJioNote>() {
        public ListJioNote createFromParcel(Parcel in) {
            return new ListJioNote(in);
        }

        public ListJioNote[] newArray(int size) {
            return new ListJioNote[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(toJSON());
        parcel.writeInt(getTargetStore());
        parcel.writeString(getBitName());
        parcel.writeInt(getVersion());
    }
    public ListJioNote() {
        noteType = NOTE_LIST;
    }
    public String toString(){
        return toJSON();
    }
}
