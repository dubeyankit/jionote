package com.jio.mhood.jionote.cache;

import android.graphics.Color;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ankit.dubey on 8/20/14.
 */
public abstract class JioNote implements Parcelable{

    public static final int NOTE_SIMPLE = 0x0001;
    public static final int NOTE_LIST = 0x0002;
    private String title;
    private String note;
    private int backGroundColor = Color.WHITE;
    private boolean isSticky;
    private boolean isDeviceOnly;
    private long reminderTime = -1;
    private long updatedAt = -1;
    protected int noteType;
    private long eventID = -1;
    //Bitstore specific parameters...
    private int targetStore;
    private String bitName;
    private int noteVersion = 1;
    private boolean override;

    public String toJSON(){
        JSONObject json = new JSONObject();
        try {
            json.put("title",title);
            json.put("text",note);
            json.put("sticky", isSticky ? 1 : 0);
            json.put("reminder",reminderTime);
            json.put("updatedAt", updatedAt);
            String hexColor = "#"+Integer.toHexString(backGroundColor).toUpperCase().substring(2);
            json.put("bgColor",hexColor);
            json.put("noteType",noteType);
            json.put("targetStore",targetStore);
            json.put("eventID",eventID);
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void fromJSON(String jsonStr){
        try {
            Log.d("JioNote",jsonStr);
            JSONObject json = new JSONObject(jsonStr);
            title = json.getString("title");
            note = json.optString("text", "");
            isSticky = (json.getInt("sticky") ==1);
            reminderTime = json.getLong("reminder");
            updatedAt = json.getLong("updatedAt");
            String hexColor = json.getString("bgColor");
            try {
                backGroundColor = Color.parseColor(hexColor);
            }
            catch(Exception e){

            }
            noteType = json.optInt("noteType",NOTE_SIMPLE);
            targetStore = json.optInt("targetStore", 2);
            eventID = json.optInt("eventID", -1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public long getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(long reminderTime) {
        this.reminderTime = reminderTime;
    }

    public JioNote(String title, String note, int backGroundColor,boolean isSticky, boolean isDeviceOnly) {
        this.title = title;
        this.note = note;
        this.backGroundColor = backGroundColor;
        this.isSticky = isSticky;
        this.isDeviceOnly = isDeviceOnly;
    }

    public JioNote() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getBackGroundColor() {
        return backGroundColor;
    }

    public void setBackGroundColor(int backGroundColor) {
        this.backGroundColor = backGroundColor;
    }


    public boolean isSticky() {
        return isSticky;
    }

    public void setSticky(boolean isSticky) {
        this.isSticky = isSticky;
    }

    public boolean isDeviceOnly() {
        return isDeviceOnly;
    }

    public void setDeviceOnly(boolean isDeviceOnly) {
        this.isDeviceOnly = isDeviceOnly;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getTargetStore() {
        return targetStore;
    }

    public void setTargetStore(int targetStore) {
        this.targetStore = targetStore;
    }

    public String getBitName() {
        return bitName;
    }

    public void setBitName(String bitName) {
        this.bitName = bitName;
    }

    public int getVersion() {
        return noteVersion;
    }

    public void setVersion(int noteVersion) {
        this.noteVersion = noteVersion;
    }

    public boolean isOverride() {
        return override;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }

    public String toString(){
        return toJSON();
    }

    public long getEventID() {
        return eventID;
    }

    public void setEventID(long eventID) {
        this.eventID = eventID;
    }
}
