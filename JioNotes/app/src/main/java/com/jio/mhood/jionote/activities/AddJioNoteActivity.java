/**
 *
 */
package com.jio.mhood.jionote.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.jio.mhood.jionote.AddNoteRequest;
import com.jio.mhood.jionote.ColorPickerDialog;
import com.jio.mhood.jionote.JioNoteApplication;
import com.jio.mhood.jionote.ListJioNoteAdapter;
import com.jio.mhood.jionote.R;
import com.jio.mhood.jionote.Utils;
import com.jio.mhood.jionote.cache.JioNote;
import com.jio.mhood.jionote.cache.SimpleJioNote;
import com.jio.mhood.services.JioServiceManager;
import com.jio.mhood.services.api.bitstore.BitstoreSettingManager;
import com.jio.mhood.services.api.bitstore.common.BitStoreTarget;
import com.jio.mhood.services.api.bitstore.common.Item;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * AddJioNoteActivity.
 */
public class AddJioNoteActivity extends Activity  implements ColorPickerDialog.OnColorChangedListener,ActionBar.OnNavigationListener  {

    private static final String TAG = "AddJioNoteActivity";

    private int backgroundColor = Color.WHITE;
    private TextView remindMe;
    private TextView date;
    private TextView time;
    private TextView lastEdited;
    private ImageView cancel;
    private JioNote jioNote;
    private int noteType = JioNote.NOTE_SIMPLE;
    private ListJioNoteAdapter listJioNoteAdapter;

    private BitstoreSettingManager bitstoreSettingManager;

    private int selectedStore = BitStoreTarget.BitStore_APP_USER;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.GINGERBREAD) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setBackgroundDrawable(new ColorDrawable(0xffcdcdcd));
        }
        Intent i = getIntent();
        jioNote = (JioNote) i.getParcelableExtra("JIO_NOTE");
        noteType = i.getIntExtra("NOTE_TYPE",JioNote.NOTE_SIMPLE);

        setContentView(R.layout.activity_add_note);

        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setBackgroundDrawable(new ColorDrawable(0xffcdcdcd));
        final String[] dropdownValues = {"Global","User","Device"};

        // Specify a SpinnerAdapter to populate the dropdown list.
        ArrayAdapter<String> optionAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, android.R.id.text1,
                dropdownValues);

        optionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set up the dropdown list navigation in the action bar.
        actionBar.setListNavigationCallbacks(optionAdapter, this);

        actionBar.setSelectedNavigationItem(1);
        EditText title = (EditText) findViewById(R.id.title);
        JioNoteApplication application = (JioNoteApplication) getApplication();
        application.setTypeface(title);
        EditText note = (EditText) findViewById(R.id.note);
        application.setTypeface(note);
        String sharedNote = i.getStringExtra("SHARED_TEXT");
        if(sharedNote != null) {
            note.setText(sharedNote);
        }

        remindMe = (TextView) this.findViewById(R.id.remind_me);
        application.setTypeface(remindMe);
        remindMe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                remindMe.setVisibility(View.GONE);
                RelativeLayout reminderLayout = (RelativeLayout)findViewById(R.id.reminderLayout);
                reminderLayout.setVisibility(View.VISIBLE);
                reminder = Calendar.getInstance();
            }
        });
        date = (TextView) this.findViewById(R.id.date);
        date.setPaintFlags(date.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        time = (TextView) this.findViewById(R.id.time);
        time.setPaintFlags(date.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        });
        lastEdited = (TextView) this.findViewById(R.id.last_edited);
        cancel = (ImageView) this.findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                remindMe.setVisibility(View.VISIBLE);
                RelativeLayout reminderLayout = (RelativeLayout)findViewById(R.id.reminderLayout);
                reminderLayout.setVisibility(View.GONE);
                reminder = null;
                time.setText(getString(R.string.time));
                date.setText(getString(R.string.date));
            }
        });


        if(jioNote != null){
            actionBar.setSelectedNavigationItem(getNavigationItemSelected(jioNote.getTargetStore()));
            title.setText(jioNote.getTitle());
            title.setSelection(title.getText().length());
            note.setText(jioNote.getNote());
            backgroundColor = jioNote.getBackGroundColor();
            selectedStore = jioNote.getTargetStore();
            lastEdited.setText(getString(R.string.edited)+" " +Utils.getLastEditedString(jioNote.getUpdatedAt()));
            ((CheckBox)findViewById(R.id.sticky)).setChecked(jioNote.isSticky());
            if(jioNote.getReminderTime() > 0){
                remindMe.setVisibility(View.GONE);
                RelativeLayout reminderLayout = (RelativeLayout)findViewById(R.id.reminderLayout);
                reminderLayout.setVisibility(View.VISIBLE);
                reminder = Calendar.getInstance();
                reminder.setTime(new Date(jioNote.getReminderTime()));
                date.setText(getDateString(jioNote.getReminderTime(),true));
                time.setText(getDateString(jioNote.getReminderTime(),false));
            }
        }
        if(noteType == JioNote.NOTE_LIST){
            note.setVisibility(View.GONE);
            LinearLayout listNote = (LinearLayout) findViewById(R.id.listNote);
            listNote.setVisibility(View.VISIBLE);
            final ListView listNoteView = (ListView) findViewById(R.id.listItems);
            listJioNoteAdapter = new ListJioNoteAdapter(AddJioNoteActivity.this,new ArrayList<String>());
            listNoteView.setAdapter(listJioNoteAdapter);
            final EditText listNoteText = (EditText)findViewById(R.id.listNoteText);
            listNoteText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if(actionId== EditorInfo.IME_ACTION_DONE){
                        String listNote = listNoteText.getText().toString();
                        listNoteText.setText("");
                        listJioNoteAdapter.add(listNote);
                        listJioNoteAdapter.notifyDataSetChanged();
                    }
                    return false;
                }
            });
        }
        setBackgroundColor(backgroundColor);
    }

    private String getDateString(long time,boolean isDate){

        if(isDate) {
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(time);
            return formattedDate;
        }
        else{
            SimpleDateFormat df = new SimpleDateFormat("hh:mm:a");
            String formattedDate = df.format(time);
            return formattedDate;
        }
    }

    public void colorChanged(int color) {
        backgroundColor = color;
        setBackgroundColor(color);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_add_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onNavigationItemSelected(int position, long id) {
        int store = -1;
        switch(position){
            case 0:
                store = BitStoreTarget.BitStore_APP;
                Log.d(TAG,"BitStore_APP");
                break;
            case 1:
                store = BitStoreTarget.BitStore_APP_USER;
                Log.d(TAG,"BitStore_APP_USER");
                break;
            case 2:
                store = BitStoreTarget.BitStore_APP_USER_DEVICE;
                Log.d(TAG,"BitStore_APP_USER_DEVICE");
                break;
        }
        if(jioNote != null && jioNote.getTargetStore() != store) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.store_alert),
                    Toast.LENGTH_LONG).show();
            getActionBar().setSelectedNavigationItem(getNavigationItemSelected(jioNote.getTargetStore()));
            return true;
        }


        if(selectedStore != store){
            selectedStore = store;
        }
        return true;
    }

    public int getNavigationItemSelected(int store) {
        switch(store){
            case BitStoreTarget.BitStore_APP:
                return 0;
            case BitStoreTarget.BitStore_APP_USER_DEVICE:
                return 2;
            case BitStoreTarget.BitStore_APP_USER:
            default:
                return 1;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_save:
                if(jioNote == null) {
                    saveNote();
                }
                else{
                    updateNote();
                }
                return true;
            case R.id.action_share:
                String title = ((TextView)findViewById(R.id.title)).getText().toString();
                String note = ((TextView)findViewById(R.id.note)).getText().toString();
                if((title != null && title.length()>0) || (note != null && note.length()>0)) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,title+ "\n"+note);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
                else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_note),
                            Toast.LENGTH_LONG).show();
                }


                return true;
            case R.id.action_color:
                int color = Color.WHITE;
                if(jioNote != null)
                    color = jioNote.getBackGroundColor();
                new ColorPickerDialog(this, this,color).show();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        bitstoreSettingManager = (BitstoreSettingManager) JioServiceManager
                .getJioService(JioServiceManager.BITSTORE_MANAGER);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(bitstoreSettingManager != null)
            bitstoreSettingManager.doDisconnect();
    }

    private void showDateDialog(){

        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(AddJioNoteActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        reminder.set(year, monthOfYear, dayOfMonth);
                        date.setText(getDateString(reminder.getTimeInMillis(),true));
                    }
                }, year, month, day);
        datePicker.setTitle(getString(R.string.select_date));
        datePicker.show();
    }

    private Calendar reminder;
    private void showTimeDialog(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddJioNoteActivity.this, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                reminder.set(Calendar.HOUR_OF_DAY,selectedHour);
                reminder.set(Calendar.MINUTE,selectedMinute);

                time.setText(getDateString(reminder.getTimeInMillis(),false));
            }
        }, hour, minute, true);
        mTimePicker.setTitle(getResources().getString(R.string.select_time));
        mTimePicker.show();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


    private void saveNote(){
        String title = ((TextView)findViewById(R.id.title)).getText().toString();
        String note = ((TextView)findViewById(R.id.note)).getText().toString();
        if((title != null && title.length()>0) || (note != null && note.length()>0)) {
            SimpleJioNote jioNote = new SimpleJioNote();
            jioNote.setTitle(title);
            jioNote.setNote(note);
            jioNote.setTargetStore(selectedStore);
            jioNote.setBackGroundColor(backgroundColor);
            boolean isSticky = ((CheckBox)findViewById(R.id.sticky)).isChecked();
            jioNote.setSticky(isSticky);
            if(reminder != null) {
                jioNote.setReminderTime(reminder.getTimeInMillis());
                setReminder(jioNote);
            }
            String bitName = "Key"+System.currentTimeMillis();
            jioNote.setUpdatedAt(System.currentTimeMillis());
            String bitBlob = jioNote.toJSON();
            boolean isOverride = (iVersion>1);
            Item setUserBitItem = new Item(bitName, bitBlob, iVersion,isOverride);
            doSetItemWebRequest(setUserBitItem);
        }
        else{
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_note),
                    Toast.LENGTH_LONG).show();
        }
    }
    int iVersion = 1;


    private void updateNote(){
        String title = ((TextView)findViewById(R.id.title)).getText().toString();
        String note = ((TextView)findViewById(R.id.note)).getText().toString();

        if((title != null && title.length()>0) || (note != null && note.length()>0)) {
            jioNote.setTitle(title);
            jioNote.setNote(note);
            jioNote.setUpdatedAt(System.currentTimeMillis());
            if(reminder != null) {
                jioNote.setReminderTime(reminder.getTimeInMillis());
                setReminder(jioNote);
            }
            jioNote.setBackGroundColor(backgroundColor);
            boolean isSticky = ((CheckBox)findViewById(R.id.sticky)).isChecked();
            jioNote.setSticky(isSticky);
            Log.d(TAG,"UpdateNoteRequest--> Key: "+jioNote.getBitName()+" Version: "+jioNote.getVersion());
            Log.d(TAG,"UpdateNoteRequest--> "+jioNote.toJSON());
            String bitBlob = jioNote.toJSON();
            Item setUserBitItem = new Item(jioNote.getBitName(), bitBlob, jioNote.getVersion(),true );
            doSetItemWebRequest(setUserBitItem);
        }
        else{
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_note),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(color);
    }



    @Override
    protected void onResume() {
        super.onResume();
    }

    /**Set items data web request code
     * @param setUserBitItem TODO*/
    public void doSetItemWebRequest(final Item setUserBitItem){
        AddNoteRequest.AddNoteResponseHandler responseHandler = new AddNoteRequest.AddNoteResponseHandler(){

            @Override
            public void onSuccess(JioNote note) {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                AddJioNoteActivity.this.finish();
            }

            @Override
            public void onFailed(int errorCode, Object object) {
                switch(errorCode){
                    case AddNoteRequest.ERROR_CONFLICT:
                        handleConflict((Item)object);
                        break;
                    case AddNoteRequest.ERROR_NOT_FOUND:
                        handleNotFoundError((String) object);
                        break;
                    case AddNoteRequest.ERROR_OTHER:
                        handleGeneralError((String) object);
                        break;
                }
            }
        };

        AddNoteRequest doSetItemWebRequest = new AddNoteRequest(this,bitstoreSettingManager,selectedStore,responseHandler);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            doSetItemWebRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, setUserBitItem);
        else
            doSetItemWebRequest.execute(setUserBitItem);
    }

    private void handleConflict(final Item item){
        DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(jioNote != null){
                    jioNote.setVersion(item.getVersion());
                    updateNote();
                }
                else{
                    iVersion = item.getVersion();
                    saveNote();
                }
            }
        };
        DialogInterface.OnClickListener noListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AddJioNoteActivity.this.finish();
            }
        };
        showDialog(AddJioNoteActivity.this,getResources().getString(R.string.conflict_error),getResources().getString(R.string.override_message),getResources().getString(R.string.yes),getResources().getString(R.string.no),yesListener,noListener);
    }

    private void handleReminder(final JioNote note){
        DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                AddJioNoteActivity.this.finish();
                setReminder(note);

            }
        };
        DialogInterface.OnClickListener noListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                AddJioNoteActivity.this.finish();
            }
        };
        showDialog(AddJioNoteActivity.this,getResources().getString(R.string.set_reminder),getResources().getString(R.string.reminder_message),getResources().getString(R.string.yes),getResources().getString(R.string.no),yesListener,noListener);
    }


    private void handleGeneralError(String error){
        DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(jioNote != null){
                    updateNote();
                }
                else{
                    saveNote();
                }
            }
        };
        DialogInterface.OnClickListener noListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AddJioNoteActivity.this.finish();
            }
        };
        showDialog(AddJioNoteActivity.this, getResources().getString(R.string.internal_error), getResources().getString(R.string.retry_message), getResources().getString(R.string.yes), getResources().getString(R.string.no), yesListener, noListener);

    }

    private void handleNotFoundError(String error){
        handleGeneralError(error);
    }

    public static void showDialog(Context context,String title, String message, String positive, DialogInterface.OnClickListener onPositiveClickListener){
        showDialog(context,title,message,positive,null,onPositiveClickListener,null);
    }


    public static void showDialog(Context context,String title, String message, String positive,String negative, DialogInterface.OnClickListener onPositiveClickListener,DialogInterface.OnClickListener onNegativeClickListener){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        if(positive != null)
            alertDialogBuilder.setPositiveButton(positive,onPositiveClickListener );
        if(negative != null)
            alertDialogBuilder.setNegativeButton(negative,onNegativeClickListener);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void setReminder(JioNote note){
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                Log.d(TAG, "Setting reminder :: ");
                ContentResolver cr = AddJioNoteActivity.this.getContentResolver();
                ContentValues values = new ContentValues();
                values.put(CalendarContract.Events.DTSTART, note.getReminderTime());
                values.put(CalendarContract.Events.TITLE, note.getTitle());
                values.put(CalendarContract.Events.DESCRIPTION, note.getNote());
                TimeZone timeZone = TimeZone.getDefault();
                values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
                values.put(CalendarContract.Events.CALENDAR_ID, 1);

                values.put(CalendarContract.Events.DURATION, "+P1H");

                values.put(CalendarContract.Events.HAS_ALARM, 1);

                // insert event to calendar
                Uri uri = null;
                if(note.getEventID() > 0){
                    Uri updateURI = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, note.getEventID());
                    getContentResolver().update(updateURI, values, null, null);
                }
                else {
                    uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
                    long eventID = Long.parseLong(uri.getLastPathSegment());
                    note.setEventID(eventID);
                }

            }
        }
        catch(Exception e){
            Log.d(TAG,"Error setting calender "+e.getMessage());
        }
    }



}