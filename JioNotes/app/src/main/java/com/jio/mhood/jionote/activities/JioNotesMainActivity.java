/**
 *
 */
package com.jio.mhood.jionote.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jio.mhood.jionote.AddNoteRequest;
import com.jio.mhood.jionote.JioNoteAdapter;
import com.jio.mhood.jionote.R;
import com.jio.mhood.jionote.cache.JioNote;
import com.jio.mhood.jionote.cache.JioNotesDBHelper;
import com.jio.mhood.jionote.cache.ListJioNote;
import com.jio.mhood.jionote.cache.SimpleJioNote;
import com.jio.mhood.services.JioServiceManager;
import com.jio.mhood.services.api.accounts.authentication.AuthenticationManager;
import com.jio.mhood.services.api.authorization.JioSecurityException;
import com.jio.mhood.services.api.bitstore.BitstoreSettingManager;
import com.jio.mhood.services.api.bitstore.common.BitStoreTarget;
import com.jio.mhood.services.api.bitstore.common.Item;
import com.jio.mhood.services.api.bitstore.common.JioBitStoreBit;
import com.jio.mhood.services.api.common.JioError;
import com.jio.mhood.services.api.common.JioException;
import com.jio.mhood.services.api.common.JioResponseHandler;
import com.jio.mhood.services.bitstore.api.common.error.ErrorTypes;
import com.jio.mhood.services.bitstore.api.common.exception.BitStoreConflictException;
import com.jio.mhood.services.bitstore.api.common.utility.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

/**
 * Launcher Activity.
 */
public class JioNotesMainActivity extends Activity{

    private static final String TAG = "JioNotesMainActivity";

    //

    private Dialog mServicesErrorDialog;

    private BitstoreSettingManager bitstoreSettingManager;

    public static final int REQUEST_CODE_SSO = 12345;

    private Handler mHandler = new Handler();

    ArrayList<JioNote> notes = new ArrayList<JioNote>();

    public static final int REQUEST_ADD_NOTE = 0x200;

    private JioNotesDBHelper jioNotesDBHelper = null;
    private AuthenticationManager authenticationManager;

    private BroadcastReceiver receiver;

    private String sharedText;

    private JioNoteAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //JioServiceManager.init(this, null);
        Log.d(TAG, "Start Service init");
        setContentView(R.layout.activity_main);
        registerMDKReceiver();
        jioNotesDBHelper = JioNotesDBHelper.getInstance(getApplicationContext());
        bitstoreSettingManager = (BitstoreSettingManager) JioServiceManager
                .getJioService(JioServiceManager.BITSTORE_MANAGER);
        authenticationManager = (AuthenticationManager) JioServiceManager
                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
        ListView noteList = (ListView) findViewById(R.id.notesList);
        noteList.setEmptyView(findViewById(R.id.emptyView));


        EditText quickNote = (EditText)findViewById(R.id.quickNote);
        quickNote.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    saveQuickNote();
                }
                return false;
            }
        });
        sharedText  = getIntent().getStringExtra("SHARED_TEXT");
        Log.d(TAG,"sharedText "+sharedText);
    }

    private void saveQuickNote(){
        String note = ((EditText) findViewById(R.id.quickNote)).getText().toString();
        saveQuickNote(note);
    }

    private void saveQuickNote(String note){

        if(note != null && note.length()>0) {
            SimpleJioNote jioNote = new SimpleJioNote();
            jioNote.setTitle("");
            jioNote.setNote(note);
            jioNote.setTargetStore(BitStoreTarget.BitStore_APP_USER);// TODO provide option to user to select store.

            String bitName = "Test"+System.currentTimeMillis();
            jioNote.setUpdatedAt(System.currentTimeMillis());
            String bitBlob = jioNote.toJSON();
            boolean isOverride = false;
            int iVersion = 1;
            Item setUserBitItem = new Item(bitName, bitBlob, iVersion,isOverride);
            AddNoteRequest.AddNoteResponseHandler responseHandler = new AddNoteRequest.AddNoteResponseHandler(){

                @Override
                public void onSuccess(JioNote note) {
                    ((EditText) findViewById(R.id.quickNote)).setText("");
                    notes = jioNotesDBHelper.getAllNotes();
                    refreshNotes();
                }

                @Override
                public void onFailed(int errorCode, Object object) {
                    handleGeneralError((String)object);
                }
            };

            AddNoteRequest doSetItemWebRequest = new AddNoteRequest(this,bitstoreSettingManager,jioNote.getTargetStore(),responseHandler);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
                doSetItemWebRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, setUserBitItem);
            else
                doSetItemWebRequest.execute(setUserBitItem);
        }
        else{
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_note),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void handleGeneralError(String error){
        DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                saveQuickNote();
            }
        };
        DialogInterface.OnClickListener noListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((EditText) findViewById(R.id.quickNote)).setText("");
            }
        };
        AddJioNoteActivity.showDialog(JioNotesMainActivity.this, getResources().getString(R.string.internal_error), getResources().getString(R.string.retry_message), getResources().getString(R.string.yes), getResources().getString(R.string.no), yesListener, noListener);

    }

    private void registerMDKReceiver(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(AuthenticationManager.ACTION_LOGIN_FINISHED);
        filter.addAction(AuthenticationManager.ACTION_LOGOUT_FINISHED);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (AuthenticationManager.ACTION_LOGIN_FINISHED.equals(intent.getAction())) {

                } else if (AuthenticationManager.ACTION_LOGOUT_FINISHED.equals(intent.getAction())) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            JioNotesDBHelper.getInstance(getApplicationContext()).deleteAllNote();
                        }
                    }).start();

                }
            }
        };
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        MenuItem action_profiler = menu.findItem(R.id.action_profiler);
        action_profiler.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_sync:
                fetchNotes();
                return true;
            case R.id.action_add_note:
                Intent intent = new Intent(getBaseContext(), AddJioNoteActivity.class);
                //intent.putExtra("STORE",selectedStore);
                intent.putExtra("NOTE_TYPE",JioNote.NOTE_SIMPLE);
                startActivityForResult(intent, REQUEST_ADD_NOTE);
                return true;
            case R.id.action_profiler:
                Intent intents = new Intent(getBaseContext(), JioNoteProfilerActivity.class);
                startActivity(intents);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(adapter.isDisplayDelete()){
            adapter.disableDisplayDelete();
            return;
        }
        super.onBackPressed();
    }

    /*
         * (non-Javadoc)
         * @see android.app.Activity#onPause()
         */
    @Override
    protected void onPause() {
        if ((mServicesErrorDialog != null)
                && (mServicesErrorDialog.isShowing())) {
            mServicesErrorDialog.dismiss();
            mServicesErrorDialog = null;
        }
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        int returnCode = JioServiceManager.isServicesAvailable(this);
        if (JioServiceManager.MSERVICES_AVAILABLE == returnCode) {
            bitstoreSettingManager = (BitstoreSettingManager) JioServiceManager
                    .getJioService(JioServiceManager.BITSTORE_MANAGER);
            authenticationManager = (AuthenticationManager) JioServiceManager
                    .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
            isConnected = true;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        int returnCode = JioServiceManager.isServicesAvailable(this);
        if (!(JioServiceManager.MSERVICES_AVAILABLE == returnCode)) {
            mServicesErrorDialog = JioServiceManager.getServicesErrorDialog(
                    this, returnCode,
                    JioServiceManager.REQUEST_CODE_SERVICES_ERROR);
            mServicesErrorDialog.show();
            return;
        }
        bitstoreSettingManager = (BitstoreSettingManager) JioServiceManager
                .getJioService(JioServiceManager.BITSTORE_MANAGER);
        authenticationManager = (AuthenticationManager) JioServiceManager
                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
        if(isUserLoggedin()){
            notes = jioNotesDBHelper.getAllNotes();
            refreshNotes();
        }
        if(sharedText != null){
            Intent intent = new Intent(getBaseContext(), AddJioNoteActivity.class);
            //intent.putExtra("STORE",selectedStore);
            intent.putExtra("NOTE_TYPE",JioNote.NOTE_SIMPLE);
            intent.putExtra("SHARED_TEXT",sharedText);
            startActivityForResult(intent, REQUEST_ADD_NOTE);
            sharedText = null;
        }
    }

    @Override
    protected void
            onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case JioServiceManager.REQUEST_CODE_SERVICES_ERROR:
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Jio mServices is unavailable.",
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;
            case REQUEST_ADD_NOTE:
                if (resultCode == RESULT_OK) {
                    refreshNotes();
                }
                return;
            case  REQUEST_CODE_SSO:
                if (resultCode == AuthenticationManager.RESULT_LOGIN_SUCCESS) {
                    // Logged In Enjoy.
                    requestSSO();
                } else if (resultCode == AuthenticationManager.RESULT_LOGIN_CANCELLED) {
                    // Not Logged In finish App.
                    JioNotesMainActivity.this.finish();
                }
                return;

            }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void requestSSO() {
        final String CONTEXT = "requestSSO";
        new Thread() {
            public void run() {
                try {

                    authenticationManager.requestSingleSignOn(new JioResponseHandler(){
                        @Override
                        public <T> void onSuccess(T returnResult) {
                            final String CONTEXT = "requestSSO.onSuccess";
                            Log.d(TAG, CONTEXT + "> returnResult: " + returnResult);
                            String result = (String) returnResult;
                            if (AuthenticationManager.RESULT_SUCCESS.equals(result)) {
                                mHandler.post(new Runnable() {
                                    public void run() {
                                        fetchNotes();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onActivityRequest(final Intent requestIntent, final int requestCode) {
                            final String CONTEXT = "requestSSO.onActivityRequest";
                            Log.d(TAG, CONTEXT + "> requestIntent: " + requestIntent
                                    + "; requestCode: "
                                    + requestCode);
                            mHandler.post(new Runnable() {
                                public void run() {
                                    startActivityForResult(requestIntent, REQUEST_CODE_SSO);
                                }
                            });
                        }
                    });

                } catch (final JioException je) {
                    Log.e(TAG, CONTEXT, je);
                    mHandler.post(new Runnable() {
                        public void run() {

                        }
                    });
                } catch (JioSecurityException jse) {
                    Log.e(TAG, CONTEXT, jse);
                    mHandler.post(new Runnable() {
                        public void run() {

                        }
                    });
                }
            }
        }.start();
    }


    private boolean isUserLoggedin(){
        Log.d(TAG, "AuthenticationManager "+authenticationManager);
        try{
            if(authenticationManager != null)
            authenticationManager = (AuthenticationManager) JioServiceManager
                    .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
            Log.d(TAG, "AuthenticationManager.isUserSessionActive() "+authenticationManager.isUserSessionActive());
            if(authenticationManager.isUserSessionActive()) {
                return true;
            }
            else{
                Log.d(TAG, "requestSSO() ");
                JioNotesDBHelper.getInstance(JioNotesMainActivity.this.getApplicationContext()).deleteAllNote();
                requestSSO();
            }
        }
        catch(JioException je){

        }
        return false;
    }
    private boolean isConnected = false;

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (bitstoreSettingManager != null)
                bitstoreSettingManager.doDisconnect();
        }
        catch(Exception e){

        }
    }

    /**Get all data web request code*/

    public void fetchNotes(){
        Log.d(TAG,"fetchNotes()");
        if (isUserLoggedin()) {
            Log.d(TAG,"User is Logged in.");
            FetchNoteRequest doGetAllWebRequest = new FetchNoteRequest(JioNotesMainActivity.this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
                doGetAllWebRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String());
            else
                doGetAllWebRequest.execute();
        }
    }

    private void refreshNotes(){
        final ListView noteList = (ListView) findViewById(R.id.notesList);
        final JioNote[] notesArray = notes.toArray(new JioNote[notes.size()]);
        adapter = new JioNoteAdapter(JioNotesMainActivity.this,notesArray);
        noteList.post(new Runnable() {
            @Override
            public void run() {
                noteList.setAdapter(adapter);
            }
        });

    }

    private ArrayList<JioNote>  convertToJioNotes(ArrayList<Item> items,int store){
        ArrayList<JioNote> storeNotes = new ArrayList<JioNote>();
        JioNote note = null;
        for (int i=0;i<items.size();i++){
            Item item = items.get(i);
            String jsonStr = item.getBitBlob();
            try {
                JSONObject json = new JSONObject(jsonStr);
                int noteType = json.optInt("noteType", JioNote.NOTE_SIMPLE);
                switch (noteType){
                    case JioNote.NOTE_LIST:
                        note = new ListJioNote();
                        note.fromJSON(jsonStr);
                        break;
                    case JioNote.NOTE_SIMPLE:
                        note = new SimpleJioNote();
                        note.fromJSON(jsonStr);
                        break;
                    default:
                        note = new SimpleJioNote();
                        note.fromJSON(jsonStr);
                }
                note.setBitName(item.getBitName());
                note.setTargetStore(store);
                note.setVersion(item.getVersion());
                storeNotes.add(note);
            } catch (JSONException e) {
                Log.d(TAG,"BAD BIT : "+jsonStr);
                e.printStackTrace();
            }
        }
        return storeNotes;
    }

    private void setNotes(ArrayList<JioNote> noteArrayList){
        JioNote note = null;
        if(notes == null) {
            notes = new ArrayList<JioNote>();
        }
        else
            notes.clear();
        for (int i=0;i<noteArrayList.size();i++){
            note = noteArrayList.get(i);
            if(note.getReminderTime() > 0){
                setReminder(note);
            }
            notes.add(note);
        }
        jioNotesDBHelper.deleteAllNote();
        jioNotesDBHelper.addNotes(notes);
        refreshNotes();
    }

    private void setReminder(JioNote note){
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                Log.d(TAG, "Setting reminder :: ");
                ContentResolver cr = JioNotesMainActivity.this.getContentResolver();
                ContentValues values = new ContentValues();
                values.put(CalendarContract.Events.DTSTART, note.getReminderTime());
                values.put(CalendarContract.Events.TITLE, note.getTitle());
                values.put(CalendarContract.Events.DESCRIPTION, note.getNote());
                TimeZone timeZone = TimeZone.getDefault();
                values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
                values.put(CalendarContract.Events.CALENDAR_ID, 1);
                values.put(CalendarContract.Events.DURATION, "+P1H");
                values.put(CalendarContract.Events.HAS_ALARM, 1);
                // insert event to calendar
                Uri uri = null;
                if(note.getEventID() > 0){
                    Uri updateURI = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, note.getEventID());
                    getContentResolver().update(updateURI, values, null, null);
                }
                else {
                    uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
                    long eventID = Long.parseLong(uri.getLastPathSegment());
                    note.setEventID(eventID);
                }

            }
        }
        catch(Exception e){
            Log.d(TAG,"Error setting calender "+e.getMessage());
        }
    }

    public void deleteNote(JioNote note) {
        Log.d(TAG,"deleteRecord() : "+note.toString());
        Log.d(TAG,"deleteRecord() Key : "+note.getBitName());
        Log.d(TAG,"deleteRecord() Version : "+note.getVersion());
        DeleteNoteRequest doDelItemWebRequest = new DeleteNoteRequest(JioNotesMainActivity.this);
        //String bitBlob = note.toJSON();
        //Item setUserBitItem = new Item(note.getBitName(), bitBlob, note.getVersion(),true );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            doDelItemWebRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, note);
        else
            doDelItemWebRequest.execute(note);
    }

    public void viewNote(JioNote note) {
        Log.d(TAG,"viewNote() : "+note.toString());
        Intent intent = new Intent(getBaseContext(), AddJioNoteActivity.class);
        intent.putExtra("JIO_NOTE", note);
        startActivityForResult(intent, REQUEST_ADD_NOTE);
    }

    public class FetchNoteRequest extends AsyncTask<Object, String, ArrayList<JioNote>> {

        Context dContext;

        private ProgressDialog dialog;
        ArrayList<Item> items = new ArrayList<Item>();
        Exception exception = null;

        public FetchNoteRequest(Context hContext){
            this.dContext = hContext;
            dialog = new ProgressDialog(hContext);
            dialog.setCancelable(false);
        }


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Getting notes, please wait.");
            if(!dialog.isShowing())
                dialog.show();
            super.onPreExecute();
        }


        private ArrayList<JioNote> fetchBits(int bitStore){
            ArrayList<Item> storeItems = new ArrayList<Item>();
            try {
                Bundle bundle = bitstoreSettingManager.getBits(bitStore);
                bundle.setClassLoader(JioBitStoreBit.class.getClassLoader());
                for (String key : bundle.keySet()) {
                    JioBitStoreBit jioBitStoreBit = bundle.getParcelable(key);
                    Item item = new Item(key,
                            jioBitStoreBit.getBitTextValue(),
                            jioBitStoreBit.getVersion(), false);
                    Log.d(TAG, "Key: " + key + " Value: " + jioBitStoreBit.getBitTextValue() + " Version: " + jioBitStoreBit.getVersion());
                    storeItems.add(item);
                }
            }
            catch(Exception e){
                exception = e;
            }
            return convertToJioNotes(storeItems,bitStore);
        }

        @Override
        protected ArrayList<JioNote> doInBackground(Object... params) {
            exception = null;
            ArrayList<JioNote> noteArrayList = new ArrayList<JioNote>();
            if (bitstoreSettingManager!=null ) {
                try {

                    Log.d(TAG,"Fetching Global Notes..");
                    noteArrayList.addAll(fetchBits(BitStoreTarget.BitStore_APP));
                    Log.d(TAG,"Fetching User Notes..");
                    noteArrayList.addAll(fetchBits(BitStoreTarget.BitStore_APP_USER));
                    Log.d(TAG,"Fetching Device Notes..");
                    noteArrayList.addAll(fetchBits(BitStoreTarget.BitStore_APP_USER_DEVICE));
                    setNotes(noteArrayList);
                } catch (IllegalArgumentException e) {
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch(Exception e){
                    publishProgress(e.getClass().getSimpleName(), Common.getStackTrace(e));
                    e.printStackTrace();
                }
            }else{
                publishProgress("Bitstore Manager is NULL", "Bitstore Manager is NULL");
            }
            return noteArrayList;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }


        @Override
        protected void onPostExecute(ArrayList<JioNote> result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(exception==null){
                if( result.size()>0){
                    Toast.makeText(JioNotesMainActivity.this,""+result.size()+" notes found.",Toast.LENGTH_LONG).show();
                    return;
                }
                else{
                    Toast.makeText(JioNotesMainActivity.this,"No notes found.",Toast.LENGTH_LONG).show();
                }
            }else{
                if(result.size() == 0 ){
                    if(exception instanceof JioException) {
                        JioException jioException = (JioException) exception;
                        List<JioError> listerrors = jioException.getErrors();
                        for (JioError jioError : listerrors) {
                            String errorCode = jioError.getCode();

                            if (errorCode.equalsIgnoreCase(ErrorTypes.HTTP_404_NOT_FOUND_EXCEPTION)) {
                                Toast.makeText(JioNotesMainActivity.this, "No notes found.", Toast.LENGTH_LONG).show();

                            } else if (errorCode.equalsIgnoreCase(ErrorTypes.HTTP_400_BAD_REQUEST)) {
                                Toast.makeText(JioNotesMainActivity.this, "Please try again after some time.", Toast.LENGTH_LONG).show();

                            }
                        }
                    }else{
                        Toast.makeText(JioNotesMainActivity.this, "Internal error. Please try again after some time.", Toast.LENGTH_LONG).show();

                    }
                }
            }

            super.onPostExecute(result);
        }


    }

    public class DeleteNoteRequest extends AsyncTask<Object, String, Void> {

        Context dContext;

        private ProgressDialog dialog;
        Exception exception = null;
        private JioNote note = null;

        public DeleteNoteRequest(Context hContext){
            this.dContext = hContext;
            dialog = new ProgressDialog(hContext);
            dialog.setCancelable(false);
        }


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Deleting note, please wait.");
            if(!dialog.isShowing())
                dialog.show();
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Object... params) {
            exception = null;

            if (bitstoreSettingManager!=null ) {
                try {
                    note = (JioNote) params[0];
                    String bitBlob = note.toJSON();
                    Item item = new Item(note.getBitName(), bitBlob, note.getVersion(),true );
                    /**Will throw exception in case of Conflict exception*/
                    bitstoreSettingManager.deleteKey(note.getTargetStore(),
                            item.getBitName(), item.getVersion());
                } catch (BitStoreConflictException e) {
                    exception = e;
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch (RemoteException e) {
                    exception = e;
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch (JioException e) {
                    exception = e;
                    String error = e.getErrors().toString();
                    publishProgress(e.getClass().getSimpleName(), error);
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch(Exception e){
                    publishProgress(e.getClass().getSimpleName(), Common.getStackTrace(e));
                    e.printStackTrace();
                }
            }else{
                publishProgress("Bitstore Manager is NULL", "Bitstore Manager is NULL");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }


        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(exception==null){
                    Toast.makeText(JioNotesMainActivity.this,"Note deleted successfully.",Toast.LENGTH_LONG).show();
                    jioNotesDBHelper.deleteNote(note);
                    notes = jioNotesDBHelper.getAllNotes();
                    if(notes != null){
                        refreshNotes();
                    }
                    return;
            }else{
                if(exception instanceof BitStoreConflictException){
                    BitStoreConflictException bitStoreConflictException = (BitStoreConflictException) exception;
                    Bundle conflictBundle = bitStoreConflictException.getBundle();

                    Set<String> setKeys =  conflictBundle.keySet();
                    for (String itemKey : setKeys) {
                        JioBitStoreBit jbsb = conflictBundle.getParcelable(itemKey);
                        Toast.makeText(JioNotesMainActivity.this,"Delete Item Conflict Data : "+itemKey+" \n "+jbsb, Toast.LENGTH_LONG).show();
                    }
                }
                else if(exception instanceof JioException){
                    JioException jioException = (JioException) exception;
                    List<JioError> listerrors = jioException.getErrors();
                    for (JioError jioError : listerrors) {
                        String errorCode = jioError.getCode();

                        if(errorCode.equalsIgnoreCase(ErrorTypes.HTTP_404_NOT_FOUND_EXCEPTION)){
                            Toast.makeText(JioNotesMainActivity.this,"No such note available for deletion.",Toast.LENGTH_LONG).show();

                        }
                        else if(errorCode.equalsIgnoreCase(ErrorTypes.HTTP_400_BAD_REQUEST)){
                            Toast.makeText(JioNotesMainActivity.this,"Please try again after some time.",Toast.LENGTH_LONG).show();

                        }
                    }
                }
            }

            super.onPostExecute(result);
        }


    }
}