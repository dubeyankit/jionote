package com.jio.mhood.jionote.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.jio.mhood.jionote.R;
import com.jio.mhood.services.JioServiceManager;

public class SplashActivity extends Activity implements JioServiceManager.JioServiceInitCallback {
    private static int SPLASH_TIME_OUT = 3000;
    private  String sharedText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JioServiceManager.init(this, this);
        setContentView(R.layout.activity_splash);

        if (Intent.ACTION_SEND.equals(getIntent().getAction()) && getIntent().getType() != null) {
            if ("text/plain".equals(getIntent().getType())) {
                sharedText = getIntent().getStringExtra(Intent.EXTRA_TEXT);
            }
        }
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, JioNotesMainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if(sharedText != null) {
                    i.putExtra("SHARED_TEXT", sharedText);
                }
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


    @Override
    public void onJioServicesReady() {

    }

    @Override
    public void onJioServicesFailed() {

    }
}
