package com.jio.mhood.jionote;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.jio.mhood.jionote.cache.JioNote;
import com.jio.mhood.jionote.cache.JioNotesDBHelper;
import com.jio.mhood.jionote.cache.ListJioNote;
import com.jio.mhood.jionote.cache.SimpleJioNote;
import com.jio.mhood.services.api.bitstore.BitstoreSettingManager;
import com.jio.mhood.services.api.bitstore.common.Item;
import com.jio.mhood.services.api.bitstore.common.JioBitStoreBit;
import com.jio.mhood.services.api.common.JioError;
import com.jio.mhood.services.api.common.JioException;
import com.jio.mhood.services.bitstore.api.common.error.ErrorTypes;
import com.jio.mhood.services.bitstore.api.common.exception.BitStoreConflictException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Set;

public class AddNoteRequest extends AsyncTask<Object, String, Void> {

    Context dContext;
    private ProgressDialog dialog;
    Item originalItem = new Item();
    Exception exception = null;
    String requestedKey = new String();
    private BitstoreSettingManager bitstoreSettingManager;
    private static final String TAG = "AddNoteRequest";
    private int selectedStore;
    private AddNoteResponseHandler responseHandler;

    public static final int ERROR_CONFLICT = 0x10000;
    public static final int ERROR_NOT_FOUND = 0x10001;
    public static final int ERROR_OTHER = 0x10002;
    private JioNote note = null;

    public AddNoteRequest(Context hContext,BitstoreSettingManager bitstoreSettingManager,int selectedStore,AddNoteResponseHandler responseHandler){
        this.dContext = hContext;
        this.bitstoreSettingManager = bitstoreSettingManager;
        this.selectedStore = selectedStore;
        this.responseHandler = responseHandler;
        dialog = new ProgressDialog(hContext);
        dialog.setCancelable(false);
    }


    @Override
    protected void onPreExecute() {
        dialog.setMessage("Saving note, please wait.");
        if(!dialog.isShowing())
            dialog.show();
        super.onPreExecute();
    }


    @Override
    protected Void doInBackground(Object... params) {
        exception = null;

        if (bitstoreSettingManager!=null) {
            try {

                originalItem = (Item) params[0];
                boolean result = bitstoreSettingManager.setBits(selectedStore,
                        originalItem.getBitName(),
                        new JioBitStoreBit(originalItem.getBitBlob(), originalItem
                                .getVersion()), originalItem.getOverrideFlag());
                Log.d(TAG, "Add/UpdateNoteResult--> " + result);
                if(result){
                    Bundle bundle = bitstoreSettingManager.getBits(selectedStore,
                            originalItem.getBitName());
                    bundle.setClassLoader(JioBitStoreBit.class.getClassLoader());
                    JioBitStoreBit singleGetResponse = bundle.getParcelable(originalItem.getBitName());

                    Item item = new Item(originalItem.getBitName(),
                            singleGetResponse.getBitTextValue(),
                            singleGetResponse.getVersion(), false);

                    String jsonStr = item.getBitBlob();
                    try {
                        JSONObject json = new JSONObject(jsonStr);
                        int noteType = json.getInt("noteType");
                        switch (noteType) {
                            case JioNote.NOTE_SIMPLE:
                                note = new SimpleJioNote();
                                note.fromJSON(jsonStr);

                                break;
                            case JioNote.NOTE_LIST:
                                note = new ListJioNote();
                                note.fromJSON(jsonStr);
                                break;
                        }
                        note.setBitName(item.getBitName());
                        note.setTargetStore(selectedStore);
                        note.setVersion(item.getVersion());
                        Log.d(TAG, "setItems():--> Note Version: " + note.getVersion());
                        if(originalItem.getOverrideFlag()){
                            //Update Note
                            JioNotesDBHelper.getInstance(dContext).updateNote(note);
                        }
                        else{
                            //New note
                            JioNotesDBHelper.getInstance(dContext).addNote(note);
                        }
                    }
                    catch(JSONException e){

                    }
                }
                exception = null;
                return null;
            } catch (BitStoreConflictException e) {
                exception = e;
                publishProgress(e.getClass().getSimpleName(), e.getMessage());
                e.printStackTrace();
            } catch (RemoteException e) {
                exception = e;
                publishProgress(e.getClass().getSimpleName(), e.getMessage());
                e.printStackTrace();
            } catch (JioException e) {
                exception = e;
                String error = e.getErrors().toString();
                publishProgress(e.getClass().getSimpleName(), error);
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                exception = e;
                publishProgress(e.getClass().getSimpleName(), e.getMessage());
                e.printStackTrace();
            } catch(Exception e){
                exception = e;
                publishProgress(e.getClass().getSimpleName(), e.getMessage());
                e.printStackTrace();
            }
        }else{
            publishProgress("Bitstore Manager is NULL", "Bitstore Manager is NULL");
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }


    @Override
    protected void onPostExecute(Void result) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }

        if(exception==null){
            Toast.makeText(dContext, "Note added successfully" + requestedKey, Toast.LENGTH_LONG).show();
            responseHandler.onSuccess(note);


        }else{
            if(exception instanceof BitStoreConflictException){
                BitStoreConflictException bitStoreConflictException = (BitStoreConflictException) exception;
                Bundle conflictBundle = bitStoreConflictException.getBundle();
                conflictBundle.setClassLoader(JioBitStoreBit.class.getClassLoader());
                Set<String> setKeys =  conflictBundle.keySet();
                Item item = new Item();
                for (String itemKey : setKeys) {
                    JioBitStoreBit jbsb = conflictBundle.getParcelable(itemKey);
                    item.setBitName(requestedKey);
                    item.setBitBlob(jbsb.getBitTextValue());
                    item.setVersion(jbsb.getVersion());
                }
                responseHandler.onFailed(ERROR_CONFLICT,item);

            }else if(exception instanceof JioException){
                JioException jioException = (JioException) exception;
                List<JioError> listerrors = jioException.getErrors();
                for (JioError jioError : listerrors) {
                    String errorCode = jioError.getCode();
                    requestedKey = originalItem !=null ? originalItem.getBitName() : "null";
                    if(errorCode.equalsIgnoreCase(ErrorTypes.HTTP_404_NOT_FOUND_EXCEPTION)){
                        responseHandler.onFailed(ERROR_NOT_FOUND,"No Data Available for : "+requestedKey);
                    }else if(errorCode.equalsIgnoreCase(ErrorTypes.HTTP_400_BAD_REQUEST)){
                        responseHandler.onFailed(ERROR_OTHER,"Bad Operation for: "+requestedKey);
                    }
                }
            }else if(exception instanceof IllegalArgumentException){
                responseHandler.onFailed(ERROR_OTHER,exception.getClass().getName()+": The fields are empty.");
            }
            else{
                responseHandler.onFailed(ERROR_OTHER,exception.getClass().getName()+": Internal error occurred.Please try after some time.");
            }
        }
        super.onPostExecute(result);
    }
    public  interface AddNoteResponseHandler{
        void onSuccess(JioNote note);
        void onFailed(int errorCode, Object object);
    }

}




