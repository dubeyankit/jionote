package com.jio.mhood.jionote;

import android.app.Application;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by ankitdubey on 11/13/14.
 */
public class JioNoteApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

    }

    private Typeface normalFont;
    private Typeface boldFont;
    private Typeface boldItalicFont;
    private Typeface italicFont;

    // -- Fonts -- //
    public void setTypeface(TextView textView) {
        if(textView != null) {
            if(textView.getTypeface() != null && textView.getTypeface().isBold() && textView.getTypeface().isItalic()) {
                textView.setTypeface(getBoldItalicFont());
            } else if(textView.getTypeface() != null && textView.getTypeface().isItalic()){
                textView.setTypeface(getItalicFont());
            } else if(textView.getTypeface() != null && textView.getTypeface().isBold()) {
                textView.setTypeface(getBoldFont());
            } else {
                textView.setTypeface(getNormalFont());
            }
        }
    }

    private Typeface getNormalFont() {
        if(normalFont == null) {
            normalFont = Typeface.createFromAsset(getAssets(),"fonts/DroidSerif.ttf");
        }
        return this.normalFont;
    }

    private Typeface getBoldFont() {
        if(boldFont == null) {
            boldFont = Typeface.createFromAsset(getAssets(),"fonts/DroidSerif-Bold.ttf");
        }
        return this.boldFont;
    }
    private Typeface getItalicFont() {
        if(normalFont == null) {
            normalFont = Typeface.createFromAsset(getAssets(),"fonts/DroidSerif-Italic.ttf");
        }
        return this.normalFont;
    }

    private Typeface getBoldItalicFont() {
        if(boldFont == null) {
            boldFont = Typeface.createFromAsset(getAssets(),"fonts/DroidSerif-BoldItalic.ttf");
        }
        return this.boldFont;
    }
}
