package com.jio.mhood.jionote;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.jio.mhood.jionote.activities.AddJioNoteActivity;

import java.util.ArrayList;

/**
 * Created by ankit.dubey on 11/14/14.
 */
public class ListJioNoteAdapter extends ArrayAdapter<String>{
    private AddJioNoteActivity context;
    private ArrayList<String> notes;
    private ArrayList<Boolean> noteCheckStatus;

    public ListJioNoteAdapter(AddJioNoteActivity context, ArrayList<String> notes) {
        super(context, R.layout.jionote_row_layout, notes);
        this.context = context;
        this.notes = notes;
        noteCheckStatus = new ArrayList<Boolean>();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.jionote_list_row_layout, parent, false);
        JioNoteApplication application = (JioNoteApplication) context.getApplication();

        final EditText noteValue = (EditText) rowView.findViewById(R.id.noteValue);
        noteValue.setText(notes.get(position));
        application.setTypeface(noteValue);
        noteValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    String listNote = noteValue.getText().toString();
                   if(listNote.isEmpty()){
                       notes.remove(position);
                       noteCheckStatus.remove(position);
                       notifyDataSetChanged();
                   }
                }
                return false;
            }
        });

        CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.listNoteCheck);
        checkBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                noteCheckStatus.set(position,isChecked);
            }
        });

        return rowView;
    }

    public String getItem(int position){
        return notes.get(position);
    }

    public void add(String listItem){
        notes.add(listItem);
        noteCheckStatus.add(false);
    }
}
