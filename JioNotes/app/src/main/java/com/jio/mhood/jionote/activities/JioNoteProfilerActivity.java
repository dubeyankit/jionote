/**
 *
 */
package com.jio.mhood.jionote.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jio.mhood.jionote.R;
import com.jio.mhood.jionote.cache.JioNote;
import com.jio.mhood.services.JioServiceManager;
import com.jio.mhood.services.api.bitstore.BitstoreSettingManager;
import com.jio.mhood.services.api.bitstore.common.BitStoreTarget;
import com.jio.mhood.services.api.bitstore.common.Item;
import com.jio.mhood.services.api.bitstore.common.JioBitStoreBit;
import com.jio.mhood.services.api.common.JioError;
import com.jio.mhood.services.api.common.JioException;
import com.jio.mhood.services.bitstore.api.common.error.ErrorTypes;
import com.jio.mhood.services.bitstore.api.common.exception.BitStoreConflictException;
import com.jio.mhood.services.bitstore.api.common.utility.Common;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Launcher Activity.
 */
public class JioNoteProfilerActivity extends Activity {
    private RecordProfile profile = null;
    private ArrayList<RecordProfile> recordProfiles = new ArrayList<RecordProfile>();
    private static final String TAG = "JioNoteProfilerActivity";

    private int selectedStore;

    private Dialog mServicesErrorDialog;

    private BitstoreSettingManager bitstoreSettingManager;

    ArrayList<JioNote> notes = new ArrayList<JioNote>();

    public static final int REQUEST_ADD_NOTE = 0x200;

    private int noOfRecords = 1;

    private int sizeOfRecord = 1; // in KBs


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiler);
        Button showResult = (Button)findViewById(R.id.showResult);
        showResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    sizeOfRecord = Integer.valueOf(((EditText) findViewById(R.id.sizeOfRecord)).getText().toString());
                }
                catch(NumberFormatException e){

                }
                doProfile();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_save:
                saveResult();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void doProfile(){
        profile = new RecordProfile();
        addRecord();
    }

    private String bitName;
    private String bitBlob;
    private int iVersion;
    private long timeTakenForDelete;

    private void addRecord(){
        selectedStore = BitStoreTarget.BitStore_APP_USER;
        char[] array = new char[sizeOfRecord];
        Arrays.fill(array, '*');
        bitBlob = new String(array);
        bitName = "Test"+System.currentTimeMillis();
        boolean isOverride = false;
        iVersion = 1;
        Item setUserBitItem = new Item(bitName, bitBlob, iVersion,isOverride);


        AddRequest doSetItemWebRequest = new AddRequest(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            doSetItemWebRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, setUserBitItem);
        else
            doSetItemWebRequest.execute(setUserBitItem);

    }

    private void handleGeneralError(String error){
        DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        };
        AddJioNoteActivity.showDialog(JioNoteProfilerActivity.this,getResources().getString(R.string.internal_error),error,getResources().getString(R.string.ok),yesListener);

    }



     private void setBitStore(int position) {
        //"Global","User","Device"
        int store = -1;
        switch(position){
            case 0:
                store = BitStoreTarget.BitStore_APP;
                Log.d(TAG,"BitStore_APP");
                break;
            case 1:
                store = BitStoreTarget.BitStore_APP_USER;
                Log.d(TAG,"BitStore_APP_USER");
                break;
            case 2:
                store = BitStoreTarget.BitStore_APP_USER_DEVICE;
                Log.d(TAG,"BitStore_APP_USER_DEVICE");
                break;
        }
    }

    /*
     * (non-Javadoc)
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
        if ((mServicesErrorDialog != null)
                && (mServicesErrorDialog.isShowing())) {
            mServicesErrorDialog.dismiss();
            mServicesErrorDialog = null;
        }
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        int returnCode = JioServiceManager.isServicesAvailable(this);
        if (JioServiceManager.MSERVICES_AVAILABLE == returnCode) {
            bitstoreSettingManager = (BitstoreSettingManager) JioServiceManager
                    .getJioService(JioServiceManager.BITSTORE_MANAGER);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        int returnCode = JioServiceManager.isServicesAvailable(this);
        if (!(JioServiceManager.MSERVICES_AVAILABLE == returnCode)) {
            mServicesErrorDialog = JioServiceManager.getServicesErrorDialog(
                    this, returnCode,
                    JioServiceManager.REQUEST_CODE_SERVICES_ERROR);
            mServicesErrorDialog.show();
            return;
        }
    }

    @Override
    protected void
            onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case JioServiceManager.REQUEST_CODE_SERVICES_ERROR:
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Jio mServices is unavailable.",
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;

            }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if(bitstoreSettingManager != null)
            bitstoreSettingManager.doDisconnect();
    }

    public void deleteRecord() {
        DeleteRequest doDelItemWebRequest = new DeleteRequest(JioNoteProfilerActivity.this);
        Item setUserBitItem = new Item(bitName, bitBlob, iVersion,true );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            doDelItemWebRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, setUserBitItem);
        else
            doDelItemWebRequest.execute(setUserBitItem);
    }

    public void saveResult() {
        SaveResult saveRequest = new SaveResult(JioNoteProfilerActivity.this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            saveRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        else
            saveRequest.execute((Void)null);
    }



    public class DeleteRequest extends AsyncTask<Object, String, Void> {

        Context dContext;

        private ProgressDialog dialog;
        Exception exception = null;

        public DeleteRequest(Context hContext){
            this.dContext = hContext;
            dialog = new ProgressDialog(hContext);
            dialog.setCancelable(false);
        }


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Deleting note, please wait.");
            if(!dialog.isShowing())
                dialog.show();
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Object... params) {
            exception = null;

            if (bitstoreSettingManager!=null ) {
                try {
                    Item item = (Item) params[0];
                     /**Will throw exception in case of Conflict exception*/
                    long deleteStartTime = System.currentTimeMillis();
                    bitstoreSettingManager.deleteKey(selectedStore,
                            item.getBitName(), item.getVersion());
                    long deleteEndTime = System.currentTimeMillis();
                    timeTakenForDelete = deleteEndTime-deleteStartTime;
                    profile.deleteTime = timeTakenForDelete;
                    recordProfiles.add(profile);
                    profile = null;
                    Log.d(TAG,"Time Taken for Delete : "+timeTakenForDelete+" ms.");

                } catch (BitStoreConflictException e) {
                    exception = e;
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch (RemoteException e) {
                    exception = e;
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch (JioException e) {
                    exception = e;
                    String error = e.getErrors().toString();
                    publishProgress(e.getClass().getSimpleName(), error);
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch(Exception e){
                    publishProgress(e.getClass().getSimpleName(), Common.getStackTrace(e));
                    e.printStackTrace();
                }
            }else{
                publishProgress("Bitstore Manager is NULL", "Bitstore Manager is NULL");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }


        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(exception==null){
                ((TextView)findViewById(R.id.deleteTime)).setText("Time taken for delete Record : "+timeTakenForDelete+" ms.");

                return;
            }else{
                if(exception instanceof BitStoreConflictException){
                    BitStoreConflictException bitStoreConflictException = (BitStoreConflictException) exception;
                    Bundle conflictBundle = bitStoreConflictException.getBundle();

                    Set<String> setKeys =  conflictBundle.keySet();
                    for (String itemKey : setKeys) {
                        JioBitStoreBit jbsb = conflictBundle.getParcelable(itemKey);
                        Toast.makeText(JioNoteProfilerActivity.this,"Delete Item Conflict Data : "+itemKey+" \n "+jbsb, Toast.LENGTH_LONG).show();
                    }
                }
                else if(exception instanceof JioException){
                    JioException jioException = (JioException) exception;
                    List<JioError> listerrors = jioException.getErrors();
                    for (JioError jioError : listerrors) {
                        String errorCode = jioError.getCode();

                        if(errorCode.equalsIgnoreCase(ErrorTypes.HTTP_404_NOT_FOUND_EXCEPTION)){
                            Toast.makeText(JioNoteProfilerActivity.this,"No such note available for deletion.",Toast.LENGTH_LONG).show();

                        }
                        else if(errorCode.equalsIgnoreCase(ErrorTypes.HTTP_400_BAD_REQUEST)){
                            Toast.makeText(JioNoteProfilerActivity.this,"Please try again after some time.",Toast.LENGTH_LONG).show();

                        }
                    }
                }
            }

            super.onPostExecute(result);
        }


    }

    public class AddRequest extends AsyncTask<Object, String, Void> {

        Context dContext;
        private ProgressDialog dialog;
        Item originalItem = new Item();
        Exception exception = null;
        String requestedKey = new String();
        long timeTakenAddUpdate;
        long timeTakenForFetch;
        private static final String TAG = "AddRequest";

        public AddRequest(Context hContext){
            this.dContext = hContext;
            dialog = new ProgressDialog(hContext);
            dialog.setCancelable(false);
        }


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Saving Record, please wait.");
            if(!dialog.isShowing())
                dialog.show();
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Object... params) {
            exception = null;

            if (bitstoreSettingManager!=null) {
                try {

                    originalItem = (Item) params[0];
                    long startTime = System.currentTimeMillis();
                    Log.d(TAG," selectedStore "+selectedStore);
                    Log.d(TAG," BitName "+originalItem.getBitName());
                    Log.d(TAG," BitBlob "+originalItem.getBitBlob());
                    Log.d(TAG," Version "+originalItem.getVersion());
                    boolean result = bitstoreSettingManager.setBits(selectedStore,
                            originalItem.getBitName(),
                            new JioBitStoreBit(originalItem.getBitBlob(), originalItem
                                    .getVersion()), originalItem.getOverrideFlag());
                    long endTime = System.currentTimeMillis();
                    timeTakenAddUpdate = endTime-startTime;
                    profile.addTime = timeTakenAddUpdate;

                    Log.d(TAG, "Add/Update Result--> " + result);
                    Log.d(TAG, "Add/Update Time --> " + timeTakenAddUpdate+" ms.");
                    if(result){
                        startTime = System.currentTimeMillis();
                        Bundle bundle = bitstoreSettingManager.getBits(selectedStore,
                                originalItem.getBitName());
                        endTime = System.currentTimeMillis();
                        timeTakenForFetch = endTime-startTime;
                        Log.d(TAG, "Fetch Time --> " + timeTakenForFetch+" ms.");
                        profile.fetchTime = timeTakenForFetch;

                        bundle.setClassLoader(JioBitStoreBit.class.getClassLoader());
                        JioBitStoreBit singleGetResponse = bundle.getParcelable(originalItem.getBitName());

                        Item item = new Item(originalItem.getBitName(),
                                singleGetResponse.getBitTextValue(),
                                singleGetResponse.getVersion(), false);
                        iVersion = item.getVersion();
                        bitName = item.getBitName();
                        bitBlob = item.getBitBlob();
                        Log.d(TAG, "Bit Version: " + item.getVersion());
                        Log.d(TAG, "Bit Name: " + item.getBitName());
                        Log.d(TAG, "Bit Size: " + item.getBitBlob().length());
                        profile.recordSize = item.getBitBlob().getBytes().length;

                    }
                    exception = null;
                    return null;
                } catch (BitStoreConflictException e) {
                    exception = e;
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch (RemoteException e) {
                    exception = e;
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch (JioException e) {
                    exception = e;
                    String error = e.getErrors().toString();
                    publishProgress(e.getClass().getSimpleName(), error);
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    exception = e;
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                } catch(Exception e){
                    exception = e;
                    publishProgress(e.getClass().getSimpleName(), e.getMessage());
                    e.printStackTrace();
                }
            }else{
                publishProgress("Bitstore Manager is NULL", "Bitstore Manager is NULL");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }


        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            if(exception==null){
                Toast.makeText(dContext, "Note added successfully" + requestedKey, Toast.LENGTH_LONG).show();
                ((TextView)findViewById(R.id.addTime)).setText("Time taken for add Record : " + timeTakenAddUpdate + " ms.");
                ((TextView)findViewById(R.id.fetchTime)).setText("Time taken for fetch Record : " + timeTakenForFetch + " ms.");
                ((TextView)findViewById(R.id.bitSize)).setText("Record Size (Byte) : "+bitBlob.getBytes().length);
                deleteRecord();

            }else{
                if(exception instanceof BitStoreConflictException){
                    BitStoreConflictException bitStoreConflictException = (BitStoreConflictException) exception;
                    Bundle conflictBundle = bitStoreConflictException.getBundle();
                    conflictBundle.setClassLoader(JioBitStoreBit.class.getClassLoader());
                    Set<String> setKeys =  conflictBundle.keySet();
                    Item item = new Item();
                    for (String itemKey : setKeys) {
                        JioBitStoreBit jbsb = conflictBundle.getParcelable(itemKey);
                        item.setBitName(requestedKey);
                        item.setBitBlob(jbsb.getBitTextValue());
                        item.setVersion(jbsb.getVersion());
                    }
                    Toast.makeText(JioNoteProfilerActivity.this,"Please try again after some time."+exception.toString(),Toast.LENGTH_LONG).show();


                }else if(exception instanceof JioException){
                    JioException jioException = (JioException) exception;
                    List<JioError> listerrors = jioException.getErrors();
                    for (JioError jioError : listerrors) {
                        String errorCode = jioError.getCode();
                        requestedKey = originalItem !=null ? originalItem.getBitName() : "null";
                        if(errorCode.equalsIgnoreCase(ErrorTypes.HTTP_404_NOT_FOUND_EXCEPTION)){
                            Toast.makeText(JioNoteProfilerActivity.this,"Please try again after some time. HTTP_404_NOT_FOUND_EXCEPTION",Toast.LENGTH_LONG).show();

                            //responseHandler.onFailed(ERROR_NOT_FOUND,"No Data Available for : "+requestedKey);
                        }else if(errorCode.equalsIgnoreCase(ErrorTypes.HTTP_400_BAD_REQUEST)){
                            Toast.makeText(JioNoteProfilerActivity.this,"Please try again after some time. HTTP_400_BAD_REQUEST",Toast.LENGTH_LONG).show();

                        }
                    }
                }else if(exception instanceof IllegalArgumentException){
                    Toast.makeText(JioNoteProfilerActivity.this,"Fields are empty. IllegalArgumentException",Toast.LENGTH_LONG).show();

                }
                else{
                    Toast.makeText(JioNoteProfilerActivity.this,"Please try again after some time.",Toast.LENGTH_LONG).show();

                }
            }
            super.onPostExecute(result);
        }

    }



    public class SaveResult extends AsyncTask<Void, Void, Void> {

        Context dContext;

        private ProgressDialog dialog;

        public SaveResult(Context hContext){
            this.dContext = hContext;
            dialog = new ProgressDialog(hContext);
            dialog.setCancelable(false);
        }


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Saving result in CSV file.");
            if(!dialog.isShowing())
                dialog.show();
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... params) {
            FileWriter writer;

            File root = Environment.getExternalStorageDirectory();
            File file = new File(root, "bitstore_result.csv");
            try {
                writer = new FileWriter(file);
                String line = String.format("%s,%s,%s,%s\n", "Bit Size", "Add Time (ms)", "Fetch Time (ms)", "Delete Time (ms)");
                writer.write(line);
                for (RecordProfile profile:recordProfiles){
                    writer.write(profile.toString());
                }
                writer.flush();
                writer.close();
            }
            catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }


    }

    public class RecordProfile{
        public long recordSize;
        public long addTime;
        public long fetchTime;
        public long deleteTime;

        @Override
        public String toString() {
            return ""+recordSize+","+addTime+","+fetchTime+","+deleteTime+"\n";
        }
    }


}